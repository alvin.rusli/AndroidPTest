package com.app.testandroidp

import android.content.DialogInterface
import android.content.Intent
import android.hardware.biometrics.BiometricPrompt
import android.os.Bundle
import android.os.CancellationSignal
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_fingerprint_settings.*

class FingerprintSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fingerprint_settings)

        btn_fingerprint_add.setOnClickListener {
            startActivity(Intent(Settings.ACTION_FINGERPRINT_ENROLL))
        }
        btn_authenticate.setOnClickListener {
            val dialog = BiometricPrompt.Builder(this)
                    .setTitle("Judul")
                    .setSubtitle("Ini Subtitle")
                    .setDescription("Ini deskripsi")
                    .setNegativeButton("negatif", mainExecutor, object : DialogInterface.OnClickListener {
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            Toast.makeText(this@FingerprintSettingsActivity, "negatif: $p1", Toast.LENGTH_SHORT).show()
                        }
                    })
                    .build()

            val cancel = CancellationSignal()
            dialog.authenticate(
                    cancel,
                    mainExecutor,
                    object : BiometricPrompt.AuthenticationCallback() {
                        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult?) {
                            Toast.makeText(this@FingerprintSettingsActivity, "success: $result", Toast.LENGTH_SHORT).show()
                        }

                        override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
                            Toast.makeText(this@FingerprintSettingsActivity, "error: $errString", Toast.LENGTH_SHORT).show()
                        }

                        override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
                            Toast.makeText(this@FingerprintSettingsActivity, "help: $helpString", Toast.LENGTH_SHORT).show()
                        }

                        override fun onAuthenticationFailed() {
                            Toast.makeText(this@FingerprintSettingsActivity, "failed", Toast.LENGTH_SHORT).show()
                        }
                    })
        }
    }
}
