package com.app.testandroidp

import android.annotation.SuppressLint
import android.content.Context
import android.net.wifi.rtt.RangingRequest
import android.net.wifi.rtt.RangingResult
import android.net.wifi.rtt.RangingResultCallback
import android.net.wifi.rtt.WifiRttManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_wifirtt.*

class WifiRttActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wifirtt)

        val rtt = getSystemService(WifiRttManager::class.java)
        if (rtt != null && rtt.isAvailable) {
            val req = RangingRequest.Builder().build()
//            rtt.startRanging(
//                    req,
//                    object : RangingResultCallback() {
//                        override fun onRangingResults(p0: MutableList<RangingResult>?) {
//                            txt_wifirtt.text = "SUCCESS:\n$p0"
//                        }
//
//                        override fun onRangingFailure(p0: Int) {
//                            txt_wifirtt.text = "FAILURE:\n$p0"
//                        }
//
//                    },
//                    null
//            )
        } else {
            txt_wifirtt.text = "WifiRtt is not available"
        }
    }
}
