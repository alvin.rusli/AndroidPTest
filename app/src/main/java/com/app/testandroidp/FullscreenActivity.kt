package com.app.testandroidp

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.activity_fullscreen.*


class FullscreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen)

        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        val attrib = window.attributes
        attrib.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES

        txt_fullscreen.post {
            val rectangle = Rect()
            val window = window
            window.decorView.getWindowVisibleDisplayFrame(rectangle)
            val statusBarHeight = rectangle.top
            txt_fullscreen.text = "status bar height: $statusBarHeight"

            val textParams = txt_fullscreen_under.layoutParams as ConstraintLayout.LayoutParams
            textParams.topMargin = statusBarHeight
            txt_fullscreen_under.layoutParams = textParams

            val overlayParams = view_safe_area.layoutParams as ConstraintLayout.LayoutParams
            overlayParams.width = rectangle.width()
            overlayParams.height = rectangle.height()
            overlayParams.topMargin = rectangle.top
            overlayParams.leftMargin = rectangle.left
            view_safe_area.layoutParams = overlayParams
        }
    }
}
